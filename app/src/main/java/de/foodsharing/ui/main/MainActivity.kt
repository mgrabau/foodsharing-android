package de.foodsharing.ui.main

import android.content.Intent
import android.content.res.Configuration
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.widget.TextView
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.core.content.ContextCompat
import androidx.core.view.GravityCompat
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.tabs.TabLayout
import de.foodsharing.BuildConfig
import de.foodsharing.R
import de.foodsharing.di.Injectable
import de.foodsharing.services.PreferenceManager
import de.foodsharing.ui.base.BaseActivity
import de.foodsharing.ui.base.EventObserver
import de.foodsharing.ui.login.LoginActivity
import de.foodsharing.ui.profile.ProfileActivity
import de.foodsharing.ui.profile.ProfileActivity.Companion.EXTRA_USER_ID
import de.foodsharing.ui.settings.SettingsActivity
import de.foodsharing.utils.LINK_BASE_URL
import de.foodsharing.utils.Utils
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.nav_header.view.account_name
import javax.inject.Inject

class MainActivity : BaseActivity(), Injectable {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @Inject
    lateinit var preferences: PreferenceManager

    private val mainViewModel: MainViewModel by lazy {
        ViewModelProvider(this, viewModelFactory).get(MainViewModel::class.java)
    }

    private val pagerAdapter = MainPagerAdapter(supportFragmentManager)
    private lateinit var drawerToggle: ActionBarDrawerToggle

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setSupportActionBar(toolbar)
        supportActionBar?.apply {
            setDisplayHomeAsUpEnabled(true)
            setDisplayShowTitleEnabled(false)
        }

        drawerToggle = ActionBarDrawerToggle(this, drawer_layout, toolbar, R.string.drawer_open, R.string.drawer_close)
        drawerToggle.syncState()

        setupTabs()

        // set actions for menu items in the drawer
        nav_view.setNavigationItemSelectedListener { menuItem ->
            // close drawer when item is tapped
            drawer_layout.closeDrawers()

            when (menuItem.itemId) {
                R.id.nav_profile -> showProfile()
                R.id.nav_logout -> logout()
                R.id.nav_support -> Utils.openSupportEmail(this, R.string.support_email_subject_suffix)
                R.id.nav_shareApp -> shareApp()
                R.id.nav_Website -> openWebsite()
                R.id.nav_settings -> openSettings()
            }

            true
        }

        version_text_view.text = getString(R.string.version, BuildConfig.VERSION_NAME, BuildConfig.FLAVOR)

        bindViewModel()

        if (preferences.isSentryEnabled == null) {
            Utils.showQuestionDialog(this, getString(R.string.use_sentry_question)) { result ->
                preferences.isSentryEnabled = result
            }
        }
    }

    private fun showProfile() {
        startActivity(Intent(this, ProfileActivity::class.java).apply {
            putExtra(EXTRA_USER_ID, mainViewModel.currentUserId.value)
        })
    }

    private fun openWebsite() {
        if (!Utils.openUrlInBrowser(this, LINK_BASE_URL))
            Utils.createSnackBar(
                findViewById(android.R.id.content),
                getString(R.string.browser_not_found),
                ContextCompat.getColor(this, R.color.colorAccent),
                Snackbar.LENGTH_LONG
            ).show()
    }

    private fun shareApp() {
        val shareIntent = Intent(Intent.ACTION_SEND)
        shareIntent.type = "text/plain"
        shareIntent.putExtra(Intent.EXTRA_TEXT, getString(R.string.share_recommend_text))
        startActivity(Intent.createChooser(shareIntent, getString(R.string.share_dialog_title)))
    }

    private fun setupTabs() {
        main_pager.adapter = pagerAdapter
        main_tab_layout.setupWithViewPager(main_pager)

        val context = this
        val initiallySelectedTabIndex = 0
        val tabColor = R.color.colorPrimaryABitLighter
        val tabColorSelected = R.color.colorPrimaryLight

        main_tab_layout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {

            override fun onTabSelected(tab: TabLayout.Tab?) {
                val textView = tab?.customView as TextView
                textView.setTextColor(ContextCompat.getColor(context, tabColorSelected))
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {
                val textView = tab?.customView as TextView
                textView.setTextColor(ContextCompat.getColor(context, tabColor))
            }

            override fun onTabReselected(tab: TabLayout.Tab?) {}
        })

        for (position in 0 until pagerAdapter.count) {
            main_tab_layout.getTabAt(position)?.apply {
                val textView = LayoutInflater.from(context)
                    .inflate(pagerAdapter.getPageTextView(position), null) as TextView
                if (position == initiallySelectedTabIndex) {
                    textView.setTextColor(ContextCompat.getColor(context, tabColorSelected))
                } else {
                    textView.setTextColor(ContextCompat.getColor(context, tabColor))
                }
                customView = textView
            }
        }
    }

    private fun bindViewModel() {
        mainViewModel.currentUserName.observe(this, {
            // Show the current user name in the side bar
            nav_view.getHeaderView(0).account_name.text = it
        })

        mainViewModel.popup.observe(this, EventObserver {
            Utils.handlePopup(this, it)
        })
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        drawerToggle.onConfigurationChanged(newConfig)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                drawer_layout.openDrawer(GravityCompat.START)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    /**
     * Logs out the user and switches to the LoginActivity. This is called when the logout button
     * in the drawer is selected.
     */
    private fun logout() {
        mainViewModel.logout()

        val intent = Intent(this, LoginActivity::class.java)
        startActivity(intent)
        finish()
    }

    private fun openSettings() {
        startActivity(Intent(this, SettingsActivity::class.java))
        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left)
    }
}
