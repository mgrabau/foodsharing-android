package de.foodsharing.ui.conversations

import com.stfalcon.chatkit.commons.ImageLoader
import com.stfalcon.chatkit.dialogs.DialogsListAdapter
import de.foodsharing.R

class FsDialogListAdapter(
    imageLoader: ImageLoader
) : DialogsListAdapter<ChatkitConversation>(R.layout.item_dialog, imageLoader) {

    fun setItemsWithoutNotify(items: List<ChatkitConversation>) {
        this.items = items
    }
}
