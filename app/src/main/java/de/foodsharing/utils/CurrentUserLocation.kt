package de.foodsharing.utils

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import androidx.core.content.ContextCompat
import androidx.lifecycle.MutableLiveData
import de.foodsharing.model.Coordinate
import de.foodsharing.utils.testing.OpenForTesting

// TODO: combine with LocationFinder
@OpenForTesting
class CurrentUserLocation(val context: Context) {
    companion object {
        const val ACCESS_FINE_LOCATION_PERMISSION = Manifest.permission.ACCESS_FINE_LOCATION
        const val ACCESS_COARSE_LOCATION_PERMISSION = Manifest.permission.ACCESS_COARSE_LOCATION
    }

    var currentUserCoordinates = MutableLiveData<Coordinate>()

    private var locationCallbackCancelable: (() -> Unit)? = null

    final fun findLocation(): Coordinate {
        if (ContextCompat.checkSelfPermission(context, ACCESS_COARSE_LOCATION_PERMISSION)
            == PackageManager.PERMISSION_GRANTED &&
            ContextCompat.checkSelfPermission(context, ACCESS_FINE_LOCATION_PERMISSION)
            == PackageManager.PERMISSION_GRANTED
        ) {
            locationCallbackCancelable?.let { it() }

            val callback = LocationFinder.instance.requestLocation {
                currentUserCoordinates.postValue(Coordinate(it.latitude, it.longitude))
            }
            locationCallbackCancelable = {
                callback()
            }
        }
        return Coordinate(0.0, 0.0)
    }

    init {
        findLocation()
    }
}
