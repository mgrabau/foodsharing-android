package de.foodsharing.services

import de.foodsharing.api.ConversationsAPI
import de.foodsharing.api.UserAPI
import de.foodsharing.api.WebsocketAPI
import de.foodsharing.model.Conversation
import de.foodsharing.model.ConversationDetailResponse
import de.foodsharing.model.ConversationIdResponse
import de.foodsharing.model.ConversationListResponse
import de.foodsharing.model.ConversationMessagesResponse
import de.foodsharing.model.ConversationResponse
import de.foodsharing.model.User
import de.foodsharing.notifications.NotificationFactory
import de.foodsharing.utils.CONVERSATIONS_PER_REQUEST
import de.foodsharing.utils.MESSAGES_PER_REQUEST
import de.foodsharing.utils.MissingProfileException
import de.foodsharing.utils.testing.OpenForTesting
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.ObservableSource
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.PublishSubject
import retrofit2.HttpException
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
@OpenForTesting
class ConversationsService @Inject constructor(
    private val conversationsApi: ConversationsAPI,
    private val userAPI: UserAPI,
    private val ws: WebsocketAPI,
    private val auth: AuthService,
    private val notificationFactory: NotificationFactory
) {
    private val readEvents = PublishSubject.create<Int>()
    private val loadProfileTrigger = PublishSubject.create<Int>()
    private val newProfileEvents = loadProfileTrigger.flatMap {
        userAPI.getUser(it)
            .subscribeOn(Schedulers.io())
            .onErrorReturn { err ->
                if (err is HttpException && err.code() == 404) {
                    return@onErrorReturn User(-1, "", "", 0)
                }
                throw err
            }
    }.share()

    private fun listPage(page: Int): Observable<ConversationListResponse> {
        val overlap = if (page > 0) 1 else 0
        return conversationsApi.list(CONVERSATIONS_PER_REQUEST + overlap, page * CONVERSATIONS_PER_REQUEST - overlap)
    }

    /**
     * Returns an [Observable] of the list of conversations. The list gets updated in case new
     * messages are received via the [WebsocketAPI], a conversations is marked as read or in case
     * the list is reloaded or the next page of messages is loaded.
     * To trigger the latter the provided streams [reloadEvents] and [loadNextEvents] are observed.
     * In case of errors, the [errorHandler] is invoked which controls the retry behavior of the
     * stream.
     *
     * @param reloadEvents Observable triggering the reloading of the current list of conversations
     * @param loadNextEvents Observable triggering the load of the next page of conversations
     * @param errorHandler maps a stream of errors to retry events or cancels the stream
     */
    fun listPaged(
        reloadEvents: Observable<Any>,
        loadNextEvents: Observable<Any>,
        errorHandler: ((Observable<Throwable>) -> ObservableSource<Any>)
    ): Observable<List<Conversation>> {
        // Observable of the individual chunks of conversations
        val pagedConversations = Observable.range(1, Integer.MAX_VALUE)
            .concatMap { page ->
                loadNextEvents.take(1)
                    .flatMap {
                        listPage(page).subscribeOn(Schedulers.io()).retryWhen(errorHandler)
                    }
            }.takeUntil {
                it.conversations.size < CONVERSATIONS_PER_REQUEST + 1
            }.subscribeOn(Schedulers.io())

        // An observables of updates to the current list of conversations
        // Can either be a new page, a new message or a conversation which was marked as read
        val updates = Observable.merge(
            pagedConversations.map {
                ConversationListUpdate.NewPageUpdate(it)
            },
            ws.subscribe().retryWhen(errorHandler).flatMap { message ->
                (message as? WebsocketAPI.ConversationMessage)?.let {
                    Observable.just(ConversationListUpdate.NewMessageUpdate(it))
                } ?: Observable.empty()
            },
            readEvents.map { ConversationListUpdate.MarkedAsReadUpdate(it) },
            newProfileEvents.map { ConversationListUpdate.NewProfileUpdate(it) }
        )

        val fullConversations = auth.currentUser()
            .subscribeOn(Schedulers.io())
            .retryWhen(errorHandler)
            .switchMap { currentUser ->

            // A subject which triggers to reload the whole conversation up to a specific index
            val loadNumSubject = PublishSubject.create<Int>()

            // A mapping which applies the updates to the current list of conversations or triggers
            // loadNumSubject if this is not possible to reload the conversations
            val conversationUpdater = { current: ConversationListResponse, update: ConversationListUpdate ->
                when (update) {
                    is ConversationListUpdate.NewPageUpdate -> {
                        if (update.response.conversations.isEmpty()) {
                            current
                        } else if (current.conversations.last().id == update.response.conversations.first().id) {
                            ConversationListResponse(
                                current.conversations +
                                    update.response.conversations.subList(1, update.response.conversations.size),
                                current.profiles.union(update.response.profiles).toList()
                            )
                        } else {
                            loadNumSubject.onNext(current.conversations.size + update.response.conversations.size - 1)
                            current
                        }
                    }
                    is ConversationListUpdate.MarkedAsReadUpdate -> {
                        val newList = current.conversations.toMutableList()
                        val index = current.conversations.indexOfFirst {
                            it.id == update.id
                        }
                        if (index != -1) {
                            newList[index] = current.conversations[index].copy(hasUnreadMessages = false)
                        }
                        current.copy(conversations = newList)
                    }
                    is ConversationListUpdate.NewMessageUpdate -> {
                        val newList = current.conversations.toMutableList()
                        val index = current.conversations.indexOfFirst {
                            it.id == update.message.cid
                        }
                        if (index != -1) {
                            newList[index] = current.conversations[index].copy(
                                lastMessage = update.message.message,
                                hasUnreadMessages = update.message.message.authorId != currentUser.id
                            )
                        } else {
                            // The message is a new one, reload the whole current list
                            loadNumSubject.onNext(current.conversations.size)
                        }
                        current.copy(conversations = newList)
                    }
                    is ConversationListUpdate.NewProfileUpdate -> {
                        val newProfilesList = current.profiles.toMutableSet()
                        newProfilesList.add(update.profile)
                        current.copy(profiles = newProfilesList.toList())
                    }
                }.let { convResponse ->
                    convResponse.copy(conversations = convResponse.conversations.sortedWith(
                        compareByDescending<ConversationResponse> { it.hasUnreadMessages }
                            .thenByDescending { it.lastMessage?.sentAt }
                    ))
                }
            }

            // Loads the initial conversations and applies subsequent updates
            loadNumSubject.startWith(CONVERSATIONS_PER_REQUEST).switchMap { numItems ->
                conversationsApi.list(numItems, 0)
                    .subscribeOn(Schedulers.io())
                    .retryWhen(errorHandler)
                    .switchMap { firstConversations ->
                        updates.scan(firstConversations, conversationUpdater)
                    }.switchMap { conversationListResponse ->
                        try {
                            Observable.just(conversationListResponse.conversations.map {
                                it.toConversation(conversationListResponse.profiles.associateBy { u -> u.id })
                            })
                        } catch (error: MissingProfileException) {
                            loadProfileTrigger.onNext(error.profileId)
                            Observable.empty()
                        }
                    }
            }
        }.subscribeOn(Schedulers.io())

        return Observable.just(Any()).concatWith(reloadEvents).switchMap {
            fullConversations
        }
    }

    private fun conversationPage(conversationId: Int, lastMessageId: Int): Observable<ConversationMessagesResponse> {
        return conversationsApi.getMessages(conversationId, lastMessageId, MESSAGES_PER_REQUEST)
    }

    /**
     *
     * @param loadNextEvents Observable triggering the load of the next page of messages
     * @param errorHandler maps a stream of errors to retry events or cancels the stream
     */
    fun conversationPaged(
        conversationId: Int,
        loadNextEvents: Observable<Any>,
        errorHandler: ((Observable<Throwable>) -> ObservableSource<Any>)
    ): Observable<Conversation> {
        // Observable of the individual chunks of conversations
        val lastMessageIdSubject = BehaviorSubject.create<Int>().toSerialized()
        val pagedConversations = lastMessageIdSubject.distinct()
            .concatMap { lastMessageId ->
                loadNextEvents.take(1)
                    .flatMap {
                        conversationPage(conversationId, lastMessageId).subscribeOn(Schedulers.io())
                            .retryWhen(errorHandler)
                    }
            }.takeUntil {
                it.messages.size < MESSAGES_PER_REQUEST
            }

        // An observables of updates to the current list of conversations
        // Can either be a new page, a new message or a conversation which was marked as read
        val updates = Observable.merge(
            pagedConversations.map {
                MessageListUpdate.NewPageUpdate(it)
            },
            ws.subscribe().retryWhen(errorHandler).flatMap {
                val message = it as? WebsocketAPI.ConversationMessage
                // Only consider updates for the current conversation
                if (message?.cid == conversationId) {
                    Observable.just(MessageListUpdate.NewMessageUpdate(it))
                } else {
                    Observable.empty()
                }
            },
            newProfileEvents.map { MessageListUpdate.NewProfileUpdate(it) }
        )

        return auth.currentUser()
            .subscribeOn(Schedulers.io())
            .retryWhen(errorHandler)
            .switchMap {

                // A subject which triggers to reload the whole conversation up to a specific index
                val loadNumSubject = PublishSubject.create<Int>()

                // A mapping which applies the updates to the current list of conversations or triggers
                // loadNumSubject if this is not possible to reload the conversations
                val conversationUpdater = { current: ConversationDetailResponse, update: MessageListUpdate ->
                    when (update) {
                        is MessageListUpdate.NewPageUpdate -> {
                            if (update.page.messages.isEmpty()) {
                                current
                            } else {
                                val newMessages = (current.conversation.messages ?: emptyList()) + update.page.messages
                                val newProfiles = current.profiles.union(update.page.profiles).toList()
                                current.copy(
                                    conversation = current.conversation.copy(messages = newMessages),
                                    profiles = newProfiles
                                )
                            }
                        }
                        is MessageListUpdate.NewMessageUpdate -> {
                            val newMessageList = current.conversation.messages?.toMutableList() ?: mutableListOf()
                            newMessageList.add(0, update.message.message)
                            current.copy(conversation = current.conversation.copy(messages = newMessageList))
                        }
                        is MessageListUpdate.NewProfileUpdate -> {
                            val newProfilesList = current.profiles.toMutableSet()
                            newProfilesList.add(update.profile)
                            current.copy(profiles = newProfilesList.toList())
                        }
                    }
                }

                // Loads the initial conversations and applies subsequent updates
                loadNumSubject.startWith(MESSAGES_PER_REQUEST).switchMap { numItems ->
                    conversationsApi.get(conversationId, numItems)
                        .subscribeOn(Schedulers.io())
                        .retryWhen(errorHandler)
                        .switchMap { firstConversations ->
                            updates.scan(firstConversations, conversationUpdater)
                        }
                }
            }.switchMap { detailResponse ->
                try {
                    val conversation = detailResponse.conversation.copy(
                            messages = detailResponse.conversation
                                .messages?.distinctBy { it.id }?.sortedByDescending { it.sentAt }
                        ).toConversation(detailResponse.profiles.associateBy { it.id })
                    if (conversation.messages.isNotEmpty()) {
                        conversation.messages.last().id.let {
                            lastMessageIdSubject.onNext(it)
                        }
                    }
                    Observable.just(conversation)
                } catch (error: MissingProfileException) {
                    loadProfileTrigger.onNext(error.profileId)
                    Observable.empty()
                }
            }.subscribeOn(Schedulers.io())
    }

    fun user2conv(foodsharerId: Int): Observable<ConversationIdResponse> {
        return conversationsApi.user2conv(foodsharerId)
    }

    fun send(conversationId: Int, body: String): Completable {
        return conversationsApi.send(conversationId, body)
    }

    fun getUsers(conversationId: Int): Observable<List<User>> {
        return conversationsApi.get(conversationId, 0).map { it.profiles }
    }

    fun markAsRead(id: Int) {
        readEvents.onNext(id)
        notificationFactory.removeConversationNotification(id)
    }

    sealed class ConversationListUpdate {
        data class NewPageUpdate(
            val response: ConversationListResponse
        ) : ConversationListUpdate()

        data class MarkedAsReadUpdate(
            val id: Int
        ) : ConversationListUpdate()

        data class NewMessageUpdate(
            val message: WebsocketAPI.ConversationMessage
        ) : ConversationListUpdate()

        data class NewProfileUpdate(
            val profile: User
        ) : ConversationListUpdate()
    }

    sealed class MessageListUpdate {
        data class NewPageUpdate(
            val page: ConversationMessagesResponse
        ) : MessageListUpdate()

        data class NewMessageUpdate(
            val message: WebsocketAPI.ConversationMessage
        ) : MessageListUpdate()

        data class NewProfileUpdate(
            val profile: User
        ) : MessageListUpdate()
    }
}

private operator fun ConversationListResponse.plus(subList: List<ConversationResponse>): ConversationListResponse {
    return ConversationListResponse(
        this.conversations + subList,
        this.profiles
    )
}
