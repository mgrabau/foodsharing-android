package de.foodsharing.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

/**
 * Represents a person that is participating in foodsharing.
 * If and only if the name of users is null, they deleted their account.
 */
data class User(
    val id: Int,
    val name: String?,
    @SerializedName(
        "avatar",
        alternate = ["photo"]
    ) // TODO: remove this once the backend uses proper normalisation
    val avatar: String?,
    val sleepStatus: Int?
) : Serializable
