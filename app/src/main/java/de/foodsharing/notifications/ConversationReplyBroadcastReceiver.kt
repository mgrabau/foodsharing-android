package de.foodsharing.notifications

import android.app.NotificationManager
import android.content.Context
import android.content.Intent
import android.os.Build
import android.widget.Toast
import androidx.core.app.RemoteInput
import dagger.android.DaggerBroadcastReceiver
import de.foodsharing.R
import de.foodsharing.api.UserAPI
import de.foodsharing.di.Injectable
import de.foodsharing.services.ConversationsService
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

/**
 * Handles conversation replies via notifications. I.e. sends the message to the api and updates the notification.
 */
class ConversationReplyBroadcastReceiver : DaggerBroadcastReceiver(), Injectable {
    companion object {
        const val EXTRA_CONVERSATION_ID = "EXTRA_KEY_CONVERSATION_ID"
        const val KEY_TEXT_REPLY = "key_text_reply"
    }

    @Inject
    lateinit var service: ConversationsService

    @Inject
    lateinit var notificationFactory: NotificationFactory

    @Inject
    lateinit var userService: UserAPI

    override fun onReceive(context: Context, intent: Intent) {
        super.onReceive(context, intent)

        val notificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        val inputtedText = RemoteInput.getResultsFromIntent(intent)?.getCharSequence(KEY_TEXT_REPLY) ?: return
        val notificationId = intent.getIntExtra(EXTRA_CONVERSATION_ID, 0)

        val disposable = service.send(notificationId, inputtedText.toString())
            .andThen(userService.currentUser())
            .firstElement()
            .subscribeOn(Schedulers.io())
            .observeOn(Schedulers.io())
            .subscribe({
                notificationFactory.addMessageToConversationNotification(
                    notificationId,
                    inputtedText.toString(),
                    System.currentTimeMillis(),
                    it
                )
            }, {
                Toast.makeText(context, R.string.send_message_failed, Toast.LENGTH_LONG).show()
                if (Build.VERSION.SDK_INT >= 23) {
                    // Filter out notification by `notificationId`
                    val statusBarNotification = notificationManager.activeNotifications.firstOrNull {
                        it.id == notificationId
                    } ?: return@subscribe
                    val notification = statusBarNotification.notification
                    notificationManager.notify(notificationId, notification)
                }
            })
    }
}
